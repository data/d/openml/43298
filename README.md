# OpenML dataset: pair0004

https://www.openml.org/d/43298

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

//Add the description.md of the data file pair0004
Cause-effect is a growing database with two-variable cause-effect pairs 
created at Max-Planck-Institute for Biological Cybernetics in Tuebingen, Germany.
==================================================================================================================================================

Some pairs are highdimensional, for machine readability the relevant information about this is coded in Meta-data.

Meta-data contains the following information:

number of pair | 1st column of cause | last column of cause | 1st column of effect | last column of effect | dataset weight

The dataset weight should be used for calculating average performance of causal inference methods
to avoid a bias introduced by having multiple copies of essentially the same data (for example,
the pairs 56-63).

When you use this data set in a publication, please cite the following paper (which
also contains much more detailed information regarding this data set in the supplement):

J. M. Mooij, J. Peters, D. Janzing, J. Zscheischler, B. Schoelkopf
"Distinguishing cause from effect using observational data: methods and benchmarks"
Journal of Machine Learning Research 17(32):1-102, 2016

NOTE: pair0001 - pair0041 are taken from the UCI Machine Learning Repository:

Asuncion, A. & Newman, D.J. (2007). UCI Machine Learning Repository [http://www.ics.uci.edu/~mlearn/MLRepository.html]. Irvine, CA: University of California, School of Information and Computer Science. 

==================================================================================================================================================
Overview over all data pairs.

			var 1				var 2					dataset			ground truth

pair0001		Altitude			Temperature				DWD			->
pair0002		Altitude			Precipitation				DWD			->
pair0003		Longitude			Temperature				DWD			->
pair0004		Altitude			Sunshine hours				DWD			->

Information for pairs0004:

DWD data (Deutscher Wetterdienst)

data was taken at 349 stations

taken from
http://www.dwd.de/bvbw/appmanager/bvbw/dwdwwwDesktop/?_nfpb=true&_pageLabel=_dwdwww_klima_umwelt_klimadaten_deutschland&T82002gsbDocumentPath=Navigation%2FOeffentlichkeit%2FKlima__Umwelt%2FKlimadaten%2Fkldaten__kostenfrei%2Fausgabe__mittelwerte__node.html__nnn%3Dtrue

more recent  link (Jan 2010):
http://www.dwd.de/bvbw/appmanager/bvbw/dwdwwwDesktop/?_nfpb=true&_pageLabel=_dwdwww_klima_umwelt_klimadaten_deutschland&T82002gsbDocumentPath=Navigation%2FOeffentlichkeit%2FKlima__Umwelt%2FKlimadaten%2Fkldaten__kostenfrei%2Fausgabe__mittelwerte__node.html__nnn%3Dtrue

more recent link (Oct 2012):
http://www.dwd.de/bvbw/appmanager/bvbw/dwdwwwDesktop?_nfpb=true&_pageLabel=_dwdwww_klima_umwelt_klimadaten_deutschland&T82002gsbDocumentPath=Navigation%2FOeffentlichkeit%2FKlima__Umwelt%2FKlimadaten%2Fkldaten__kostenfrei%2Fausgabe__mittelwerte__akt__node.html%3F__nnn%3Dtrue

x: altitude

y: sunshine (yearly value averaged over 1961-1990)

ground truth:
x --> y

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43298) of an [OpenML dataset](https://www.openml.org/d/43298). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43298/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43298/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43298/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

